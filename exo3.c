#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

int main(void) {
    int test[] = {5,8,1,4,3,9,2,7,3,8,1,4,5,3,8};

    int b = 0;
    int w = 0;
    int r = 14; // n ?

    while (w <= r) {
        if (test[w] >= 3 && test[w] <=6) {
            w++;
        } else if (test[w] < 3) {
            swap(&test[b],&test[w]);

            b++;
            w++;
        } else {
            swap(&test[w],&test[r]);
            
            r--;
        }
    }

    for (int i = 0; i < 15; i++) {
        printf("%d ", test[i]);
    }

    printf("\n");

    return 0;
}